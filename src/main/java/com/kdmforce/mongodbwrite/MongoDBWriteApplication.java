package com.kdmforce.mongodbwrite;


import com.kdmforce.mongodbwrite.persistence.MongoDB;
import com.rabbitmq.client.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class MongoDBWriteApplication {


	private static final String TASK_QUEUE_NAME = "dispositiva_queue";


	public static void main(String[] args){
		try {
			getMessaToEventMonitoring(args);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getMessaToEventMonitoring(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		final Connection connection = factory.newConnection();
		final Channel channel = connection.createChannel();

		channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		channel.basicQos(1);

		final Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				try {
					JSONParser jsonParser = new JSONParser();
					MongoDB.registerInMongo((JSONObject) jsonParser.parse(message));
				} catch (ParseException e) {
					e.printStackTrace();
				} finally {
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			}
		};
		boolean autoAck = false;
		channel.basicConsume(TASK_QUEUE_NAME, autoAck, consumer);
	}
}
