package com.kdmforce.mongodbwrite.persistence;

import com.mongodb.*;
import org.json.simple.JSONObject;

import java.net.UnknownHostException;
import java.util.Set;


public class MongoDB {


    public static void registerInMongo(JSONObject jsonObject){
        try {

            /**** Connect to MongoDB ****/
            // Since 2.10.0, uses MongoClient

            MongoClient mongo = new MongoClient("localhost", 27017);

            /**** Get database ****/
            // if database doesn't exists, MongoDB will create it for you
            DB db = mongo.getDB("testDB");

            /**** Get collection / table from 'testdb' ****/
            // if collection doesn't exists, MongoDB will create it for you
            DBCollection table = db.getCollection("test");

            /**** Insert ****/
            // create a document to store key and value
            BasicDBObject document = new BasicDBObject();

            JSONObject documentjson = (JSONObject) jsonObject.get("document");
            documentjson.put("timestamp", (Long)jsonObject.get("timestamp"));

            Set<String> keys = (Set<String>) documentjson.keySet();

            for (String key : keys) {

                if(documentjson.get(key) instanceof Long) {
                    document.put(key, (Long) documentjson.get(key));
                } else if (documentjson.get(key) instanceof Integer) {
                    document.put(key, (Integer) documentjson.get(key));
                } else {
                    document.put(key, (String) documentjson.get(key));
                }

            }

            table.insert(document);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (MongoException e) {
            e.printStackTrace();
        }
    }
}
